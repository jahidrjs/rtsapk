import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { AssignmentListService } from './../../../services/decision/assignment-list.service';
import { LoadingController } from '@ionic/angular';



@Component({
  selector: 'app-available',
  templateUrl: './available.component.html',
  styleUrls: ['./available.component.scss'],
})
export class AvailableComponent implements OnInit {
  available = true;
  department = true;
  lesson = true;
  sameclass = true;
  relief = true;
  teachers: any;
  passdata: any;
  user: any;
  recomdata: any;
  selectedteac: any;
  checkbool: any;
  entries: any;
  searchTerm: any;
  constructor(public modalCtrl: ModalController, public alertController: AlertController,
    public assignservice: AssignmentListService, public loadingController: LoadingController,
    private navParams: NavParams
  ) {

  }
  ngOnInit() {
    this.checkbool = false;
    this.selectedteac = [];
    this.getteachers();
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    console.log(this.user.data.user_id);
  }

  chngcheckbox() {
    if (this.checkbool) {
      this.passdata.checkRuels[0] = (this.available == true) ? "available" : "";
      this.passdata.checkRuels[1] = (this.department == true) ? "subject" : "";
      this.passdata.checkRuels[2] = (this.lesson == true) ? "cons_class" : "";
      this.passdata.checkRuels[3] = (this.sameclass == true) ? "same_class" : "";
      this.passdata.checkRuels[4] = (this.teachers == true) ? "last_7d_relief" : "";
      console.log(this.passdata.checkRuels);
      //this.presentLoading();
      this.teachers = [];
      this.assignservice.retriveteachers(this.passdata).subscribe((response) => {
        console.log(response);
        //  this.dismissloading();
        this.teachers = response;
      });
    }
  }


  getteachers() {
    this.presentLoading();
    this.passdata = {
      "teacher_id": this.navParams.get('teacher_id'),
      "subjectCode": this.navParams.get('subjectCode'),
      "lessonDate": this.navParams.get('lessonDate'),
      "lessonStartTime": this.navParams.get('lessonStartTime'),
      "lessonEndTime": this.navParams.get('lessonEndTime'),
      "lessonduration": this.navParams.get('lessonduration'),
      "class_id": this.navParams.get('class_id'),
      "checkRuels": ["available", "subject", "cons_class", "same_class", "last_7d_relief"],
      "row_id": this.navParams.get('row_id'),
      "assign_id": this.navParams.get('assign_id'),
      "status_id": this.navParams.get('status_id')
    };
    console.log(this.passdata);
    this.assignservice.retriveteachers(this.passdata).subscribe((response) => {
      console.log(response);
      this.dismissloading();
      this.teachers = response;
      this.checkbool = true;
    });
  }

  selectrecomm(teach) {

    this.selectedteac = JSON.parse(localStorage.getItem('selectteach'));
    this.recomdata = {
      "teacher_id": teach.teacher_id,
      "userid": this.user.data.user_id,
      "row_id": teach.row_id,
      "status_id": this.passdata.status_id
    };
    if (this.selectedteac == null) {
      this.selectedteac = [];
    }
    this.selectedteac.push(this.recomdata);
    localStorage.setItem('selectteach', JSON.stringify(this.selectedteac));
    this.assignservice.selectrecom(this.recomdata).subscribe((response) => {
      this.dismiss();
      // console.log(this.recomdata);
      // console.log(response);
    });

  }

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      // cssClass: 'my-custom-class',
      message: 'Please wait...',
      //  duration: 2000
    });
    await loading.present();
  }

  async dismissloading() {
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  serchtrans() {
  }

  async presentAlert(teach) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are You Sure',
      //    subHeader: 'Subtitle',
      //    message: 'This is an alert message.',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'danger',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Okay',
        cssClass: 'secondary',
        handler: () => {
          this.selectrecomm(teach);
        }
      }]
    });

    await alert.present();
  }


}
