import { Router } from '@angular/router';
import { RejectAssignmentService } from './../../../services/decision/reject-assignment.service';
import { FormControl, FormGroup, FormsModule } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams, LoadingController, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-reject',
  templateUrl: './reject.component.html',
  styleUrls: ['./reject.component.scss'],
})

export class RejectComponent implements OnInit {
  @Input() row_id: string;
  @Input() user_id: string;
  rejectForm: FormGroup;
  rejectType: any = [];

  constructor(
    private modalCtrl: ModalController,
    public navParams: NavParams,
    private rejectServ: RejectAssignmentService,
    public loadingController: LoadingController,
    public alertShow: AlertController,
    private router: Router,
  ) { }

  ngOnInit() {
    this.initReject();
    this.rejectReason();
  }

  initReject() {
    this.rejectForm = new FormGroup({
      reasonSelect: new FormControl(''),
      otherReason: new FormControl('')
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();
  }

  async loadingdismiss() {
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  rejectReason() {
    this.presentLoading();
    this.rejectServ.retriveTypes().subscribe((data: any) => {
      this.getFiles();
    });
  }

  getFiles() {
    this.rejectType = JSON.parse(localStorage.getItem('rejectTypes'));
    console.log(this.rejectType);
    this.loadingdismiss();
  }

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }

  async presentAlert(msg) {
    const alert = await this.alertShow.create({
      header: '',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  rejectionDone() {
    const rejectValue: any = {};
    rejectValue.row_id = this.row_id;
    rejectValue.userid = this.user_id;
    rejectValue.reject_reasons_id = this.rejectForm.value.reasonSelect;
    rejectValue.comments = this.rejectForm.value.otherReason;
    return rejectValue;
  }

  submitData() {
    console.log(this.rejectionDone());
    this.presentLoading();
    this.rejectServ.retriveRequest(this.rejectionDone()).subscribe((data: any) => {
      this.loadingdismiss();
      this.presentAlert(data);
      this.dismiss();
      this.router.navigate(['/decision']);
    });
  }

}
