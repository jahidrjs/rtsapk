import { FileUploadService } from './../../services/request/file-upload.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-retrive',
  templateUrl: './retrive.component.html',
  styleUrls: ['./retrive.component.scss']
})

export class RetriveComponent implements OnInit {
  @Input() block_no: string;
  retrieveFile: FormGroup;
  totalDiv: any = [];
  item: any = [];
  userId: string;
  blockDetails: any = [];
  result: any = [];

  constructor(public modalCtrl: ModalController, private uploadFile: FileUploadService, public navParams: NavParams) {
    this.convertJson();
  }

  ngOnInit() {
    this.basicInfo();
    this.initFileUpload();
    this.totalDiv = [];
  }
  convertJson() {
    const blockJson = localStorage.getItem('blockDetails');
    this.blockDetails = JSON.parse(blockJson);
  }

  basicInfo() {
    this.item = JSON.parse(localStorage.getItem('currentUser'));
    this.userId = this.item.data.user_id;
  }

  initFileUpload() {
    this.retrieveFile = new FormGroup({
      description: new FormControl('', Validators.required),
      title: new FormControl('', Validators.required),
      uploadedFile: new FormControl('', Validators.required),
      fileSource: new FormControl('', [Validators.required])
    });
  }

  get f() {
    return this.retrieveFile.controls;
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.retrieveFile.patchValue({
        fileSource: file
      });
    }
  }

  fileUpload() {
    const formData = new FormData();
    formData.append('userid', this.userId);
    formData.append('description', this.retrieveFile.get('description').value);
    formData.append('title', this.retrieveFile.get('title').value);
    formData.append('uploadedFile', this.retrieveFile.get('fileSource').value);
    this.uploadFile.filesUpload(formData).subscribe((data: any) => {
      this.result = data;
      this.updateArray(this.block_no);
      this.dismiss();
    });
  }
  updateArray(id) {
    var index = this.blockDetails.findIndex(obj => obj.block === id);
    if (index > -1) {
      this.blockDetails[index]['uploadFile'] = this.result;
    }
    localStorage.setItem('blockDetails', JSON.stringify(this.blockDetails));
    console.log(localStorage.getItem('blockDetails'));
  }

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }

  addNewContent() {
    /*
    // const divElement = [];
    const x = document.getElementsByClassName('hiddenInput').length;
    let max = x + 1;
    for (var y = 0; y < max; y++) {
      this.totalDiv.index[y] = x;
    }
    return this.totalDiv;
    */
  }
}
