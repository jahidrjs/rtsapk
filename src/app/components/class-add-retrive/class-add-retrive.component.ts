import { FormControl, FormGroup, FormsModule } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-class-add-retrive',
  templateUrl: './class-add-retrive.component.html',
  styleUrls: ['./class-add-retrive.component.scss'],
})
export class ClassAddRetriveComponent implements OnInit {
  public currntDate: string;
  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
    const now = new Date();
    this.currntDate = now.toISOString();
  }
  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }

}
