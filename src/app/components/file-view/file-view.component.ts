import { FilePreviewService } from './../../services/decision/file-preview.service';
import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-file-view',
  templateUrl: './file-view.component.html',
  styleUrls: ['./file-view.component.scss'],
})
export class FileViewComponent implements OnInit {
  @Input() row_id: string;
  fileList: any = [];

  constructor(
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private fileViewServ: FilePreviewService,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.fileView();

  }

  fileView() {
    this.presentLoading();
    this.fileViewServ.retriveData(this.formData()).subscribe((data: any) => {
      this.getFiles();
    });
    // return JSON.parse(fileValues);
  }

  getFiles() {
    this.fileList = JSON.parse(localStorage.getItem('filesList'));
    console.log(this.fileList);
    this.loadingdismiss();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      // cssClass: 'my-custom-class',
      message: 'Please wait...',
      //  duration: 2000
    });
    await loading.present();
  }


  async loadingdismiss() {
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }


  formData() {
    const formValues: any = {};
    formValues.row_id = this.row_id;
    return formValues;
  }
  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }
}
