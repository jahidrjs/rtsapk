import { AuthGuard } from './../guards/auth.guard';
import { LoginService } from './../services/login.service';
import { Component, OnInit } from '@angular/core';
import { MenuController, AlertController } from '@ionic/angular';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public phone: number;
  loginForm: FormGroup;
  public result = [];
  submitted = false;
  returnUrl: string;

  constructor(
    private menuCtrl: MenuController,
    private router: Router,
    private route: ActivatedRoute,
    private alertShow: AlertController,
    private loginService: LoginService,
	public loadingController: LoadingController
  ) { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.logout();
    this.loginFormInit();
    this.loginService.logOut();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }


  loginFormInit(): void {
    this.loginForm = new FormGroup({
      phone: new FormControl('', Validators.required)
    });
  }

  getPhone() {
    const phoneNumber: any = {};
    phoneNumber.phone = this.loginForm.value.phone;
    return phoneNumber;
  }

  async presentAlert(msg) {
    const alert = await this.alertShow.create({
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
  login() {
    // this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loginService.logIn(this.getPhone()).subscribe((data: any) => {
      if (data.status === 1) {
        this.router.navigate(['/dashboard']);
      } else {
        this.presentAlert(data.message);
      }
    });
  }
  logout() {
    this.loginService.logOut();
  }
}
