import { RetriveComponent } from './../../components/retrive/retrive.component';
import { ClassAddRetriveComponent } from './../../components/class-add-retrive/class-add-retrive.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RetrivePageRoutingModule } from './retrive-routing.module';
import { RetrivePage } from './retrive.page';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RetrivePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RetrivePage, RetriveComponent, ClassAddRetriveComponent],
  entryComponents: [RetriveComponent, ClassAddRetriveComponent]
})
export class RetrivePageModule { }
