import { TeacherService } from './../../services/request/teacher.service';
import { ClassAddRetriveComponent } from './../../components/class-add-retrive/class-add-retrive.component';
import { RetriveComponent } from './../../components/retrive/retrive.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, MenuController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { RetriveService } from 'src/app/services/request/retrive.service';
import { IonBackButtonDelegate } from '@ionic/angular';

@Component({
  selector: 'app-retrive',
  templateUrl: './retrive.page.html',
  styleUrls: ['./retrive.page.scss'],
  providers: [DatePipe]
})
export class RetrivePage implements OnInit {
  @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;

  teacher = [];
  retValues: any = [];
  user_name: string;
  user_phon: string;
  user_id: string;
  item: any = [];
  searchDate: any = [];
  start_time: any = '';
  teachers: any;
  blockDetails: any = [];
  selectedTeacher: any = [];
  users: any = [];
  fileUpload = true;
  fileView = false;

  constructor(
    public modalCtrl: ModalController,
    public router: Router,
    public menuCtrl: MenuController,
    public datepipe: DatePipe,
    public teacherServ: TeacherService,
    public retSubmit: RetriveService,
    public loadingController: LoadingController,
    private alertShow: AlertController
  ) { }

  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    this.setUIBackButtonAction();
  }

  setUIBackButtonAction() {
    this.backButton.onClick = () => {
      this.router.navigate(['/request']);
    };
  }

  ngOnInit(): void {
    this.menuCtrl.enable(true);
    this.basicInfo();
    this.teachers = JSON.parse(localStorage.getItem('teacherList'));
    this.retriveValues();
    this.initialPeriods();
  }

  basicInfo() {
    console.log(localStorage.getItem('currentUser'));
    this.item = JSON.parse(localStorage.getItem('currentUser'));
    this.user_name = this.item.data.user_full_name;
    this.user_phon = this.item.data.phone_number;
    this.user_id = this.item.data.user_id;
  }

  retriveValues() {
    // console.log(localStorage.getItem('retrieveData'));
    this.retValues = JSON.parse(localStorage.getItem('retrieveData'));
    // console.log(localStorage.getItem('searchDate'));
    this.searchDate = JSON.parse(localStorage.getItem('searchDate'));
  }

  async openFileModal(val) {
    const modal = await this.modalCtrl.create({
      component: RetriveComponent,
      cssClass: 'retriveModal',
      componentProps: {
        "block_no": val
      }
    });
    return await modal.present();
  }

  async classAddRetrive() {
    const modal = await this.modalCtrl.create({
      component: ClassAddRetriveComponent,
      cssClass: 'retriveClassAdd'
    });
    return await modal.present();
  }

  request() {
    localStorage.removeItem('searchDate');
    localStorage.removeItem('periods');
    localStorage.removeItem('retrieveData');
    this.router.navigate(['/request']);
  }

  getData() {
    const search: any = {};
    search.userid = this.user_id;
    return search;
  }

  getLessonId(val) {
    let value = '';
    this.retValues.forEach(element => {
      const x = element.block_id;
      const z = element.lessons_id;
      if (x === val) {
        value = z;
      }
    });
    return value;
  }

  getClassroomId(val) {
    let value = '';
    this.retValues.forEach(element => {
      const x = element.block_id;
      const z = element.classrooms;
      if (x === val) {
        value = z;
      }
    });
    return value;
  }

  getClassId(val) {
    let value = '';
    this.retValues.forEach(element => {
      const x = element.block_id;
      const z = element.class_id;
      if (x === val) {
        value = z;
      }
    });
    return value;
  }

  getSubjectId(val) {
    let value = '';
    this.retValues.forEach(element => {
      const x = element.block_id;
      const z = element.subject_id;
      if (x === val) {
        value = z;
      }
    });
    return value;
  }

  getLessonDuration(val) {
    let value = '';
    this.retValues.forEach(element => {
      const x = element.block_id;
      const z = element.lession_duration;
      if (x === val) {
        value = z;
      }
    });
    return value;
  }

  getLessonDate(val) {
    let value = '';
    this.retValues.forEach(element => {
      const x = element.block_id;
      const z = element.date;
      if (x === val) {
        value = z;
      }
    });
    return value;
  }

  getLessonTime(val) {
    let value = '';
    this.retValues.forEach(element => {
      const x = element.block_id;
      const z = element.start_time;
      if (x === val) {
        value = z;
      }
    });
    return value;
  }
  getHodId(val) {
    let value = '';
    this.retValues.forEach(element => {
      const x = element.block_id;
      const z = element.hodteacher;
      if (x === val) {
        value = z;
      }
    });
    return value;
  }

  initialPeriods() {
    let id = 1;
    this.blockDetails = [];
    const total = this.retValues.length;
    for (let x = 0; x < total; x++) {
      this.blockDetails.push(this.setValuetoArray(id));
      id++;
    }
    console.log(this.blockDetails);
    localStorage.setItem('blockDetails', JSON.stringify(this.blockDetails));
  }

  setValuetoArray(id) {
    const newArray: any = {};
    var val = 0;
    newArray.block = id;
    newArray.assigned_teacher_user_id = val;
    newArray.hodteacher = this.getHodId(id);
    newArray.lessons_id = this.getLessonId(id);
    newArray.subjects_id = this.getSubjectId(id);
    newArray.lesson_date = this.getLessonDate(id);
    newArray.lesson_time = this.getLessonTime(id);
    newArray.lesson_duration = this.getLessonDuration(id);
    newArray.class_id = this.getClassId(id);
    newArray.classroom_id = this.getClassroomId(id);
    newArray.uploadFile =
      [{
        "file_title": "",
        "written_text": "",
        "file_type": "",
        "file_url": "",
        "created_by": "",
        "created_at": ""
      }];
    return JSON.parse(JSON.stringify(newArray));
  }

  updateArray(id, val) {
    var index = this.blockDetails.findIndex(obj => obj.block === id);
    if (index > -1) {
      this.blockDetails[index]['assigned_teacher_user_id'] = val;
    }
    localStorage.setItem('blockDetails', JSON.stringify(this.blockDetails));
  }

  getTeacher(val, id) {
    this.updateArray(id, val);
  }

  makeUserArray() {
    const user_info: any = {};
    this.users = JSON.stringify(localStorage.getItem('searchDate'));
    user_info.userid = this.users.userid;
    user_info.startDate = this.users.start_date;
    user_info.endDate = this.users.end_date;
    user_info.leaveid = this.users.leave_type;
    return user_info;
  }

  finalSubmit() {
    const subValue: any = {};
    console.log(localStorage.getItem('searchDate'));
    subValue.userInfo = localStorage.getItem('searchDate');
    console.log(localStorage.getItem('blockDetails'));
    subValue.periods = localStorage.getItem('blockDetails');
    return subValue;
  }

  requestDone() {
    this.presentLoading();
    this.retSubmit.submitRequest(this.finalSubmit()).subscribe((data: any) => {
      this.requestAlert(data.msg);
    });
  }
  async presentAlert(msg) {
    const alert = await this.alertShow.create({
      // cssClass: 'my-custom-class',
      header: '',
      // subHeader: 'Subtitle',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
  requestAlert(msg) {
    console.log(msg);
    this.loadingdismiss();
    this.presentAlert(msg);
    this.router.navigate(['/request']);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();
  }

  async loadingdismiss() {
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  signOut() {
    this.router.navigate(['/login']);
  }
}
