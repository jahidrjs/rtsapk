import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RetrivePage } from './retrive.page';

describe('RetrivePage', () => {
  let component: RetrivePage;
  let fixture: ComponentFixture<RetrivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetrivePage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RetrivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
