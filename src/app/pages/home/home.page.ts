import { LoginService } from './../../services/login.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { RedirectUrlService } from 'src/app/services/redirect-url.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  results: Observable<any>;
  searchTerm = '';
  user_name: string;
  user_phon: string;
  tel_user: string;
  hod: boolean = false;
  item: any = [];
  constructor(
    public router: Router,
    public menuCtrl: MenuController,
    private loginService: LoginService,
    private redirectTo: RedirectUrlService,
    private menu: MenuController
  ) { }

  ngOnInit() { }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.redirectTo.redirectTO();
    this.basicInfo();
  }

  openFirst() {
    this.basicInfo();
    this.menu.enable(true, 'mainMenu');
    this.menu.open('mainMenu');
  }

  basicInfo() {
    console.log(localStorage.getItem('currentUser'));
    this.item = JSON.parse(localStorage.getItem('currentUser'));
    this.user_name = this.item.data.user_full_name;
    this.user_phon = this.item.data.phone_number;
    this.tel_user = this.item.data.tg_chat_id;
    this.hod = (this.item.data.hod === 1) ? true : false;
  }

  pageUrl(url) {
    switch (url) {
      case 1: this.router.navigate(['/request']); break;
      case 2: this.router.navigate(['/assignment']); break;
      case 3: this.router.navigate(['/decision']); break;
      default: this.router.navigate(['/dashboard']); break;
    }
  }
  logout() {
    this.loginService.logOut();
  }
}
