import { HistoryComponent } from './../../components/assignment/history/history.component';
import { ChangeTeaComponent } from './../../components/assignment/change-tea/change-tea.component';
import { AvailableComponent } from './../../components/assignment/available/available.component';
import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { AssignmentListService } from './../../services/decision/assignment-list.service';
import { LoadingController } from '@ionic/angular';
import { LoginService } from './../../services/login.service';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.page.html',
  styleUrls: ['./assignment.page.scss'],
})
export class AssignmentPage implements OnInit {
  assignments: any;
  passdata: any;
  searchTerm: any;
  tempassign: any;
  user: any;
  selectedteac: any;
  selectedstatus: any;
  constructor(
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public assignservice: AssignmentListService,
    private loginService: LoginService,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
    this.selectedteac = JSON.parse(localStorage.getItem('selectteach'));
    console.log(this.selectedteac);
    //this.available();
    this.presentLoading();
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    setTimeout(() => {
      this.getassignments(this.user.data.user_id);
    }, 200);
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      // cssClass: 'my-custom-class',
      message: 'Please wait...',
      //  duration: 2000
    });
    await loading.present();
  }

  async dismissloading() {
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }


  getassignments(id) {
    this.assignservice.retriveassign(id.toString()).subscribe((response) => {
      console.log(response);
      if (response) {
        this.dismissloading();
      }
      this.assignments = response;
      localStorage.setItem('tempassignments', JSON.stringify(this.assignments));
      // this.submitDataList(this.assignments.data);
    });
  }

  gettempassign() {
    return this.tempassign;
  }

  submitDataList(arrayValues) {
    // const sendValues: any = {};
    // const dataArr: any = {};
    var sendValues = [], dataArr: any = {};
    const userId = this.user.data.user_id;
    arrayValues.forEach(function (value) {
      if (value.status_id == 1 || value.status_id == 6) {
        dataArr.teacher_id = value.assigned_id;
        dataArr.row_id = value.absence_request_table_id;
        dataArr.userid = userId;
        sendValues.push(dataArr);
      }
    });
    return sendValues;
  }

  getSendValues() {
    this.assignments = JSON.parse(localStorage.getItem('tempassignments'));
    const values = this.submitDataList(this.assignments.data);
    return values;
  }

  setFilteredItems(searchTerm: any) {
    this.assignments = JSON.parse(localStorage.getItem('tempassignments'));
    this.assignments.data =
      this.assignments.data.filter((pro: any) => {
        return (pro.subject_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
      });
  }

  makeSendValues() {
    const sendValues: any = {};
    this.assignments = JSON.parse(localStorage.getItem('tempassignments'));
    sendValues.data = this.assignments.data;
    sendValues.userid = this.user.data.user_id;
    return sendValues;
  }

  sendrequest() {
    // console.log(this.getSendValues());
    // this.assignments = JSON.parse(localStorage.getItem('tempassignments'));
    console.log(this.makeSendValues());
    this.assignservice.sendreq(this.makeSendValues()).subscribe((response) => {
      // console.log(response);
      var message = response['msg'];
      alert(message);
      // console.log(response);
      if (response) {
        this.presentLoading();
        this.getassignments(this.user.data.user_id);
      }
      localStorage.setItem('selectteach', null);
    });
    /*
    this.selectedteac = this.assignments; // JSON.parse(localStorage.getItem('selectteach'));
    console.log(this.selectedteac);
    if (this.selectedteac == null) {
      alert("No Teacher selected");
    }
    else {
      var data = {
        "data": [
        ]
      };
      data.data = this.selectedteac;
      console.log(data);
      this.assignservice.sendreq(data).subscribe((response) => {
        if (response) {
          this.presentLoading();
          this.getassignments(this.user.data.user_id);
        }
        localStorage.setItem('selectteach', null);
      });
    }
    */
  }


  async available(singteach) {
    console.log(singteach);
    //console.log(singteach);
    const modal = await this.modalCtrl.create({
      component: AvailableComponent,
      cssClass: 'availableModal',
      componentProps: {
        "teacher_id": singteach.sub_teacher,
        "subjectCode": singteach.subjects_id,
        "lessonDate": singteach.lesson_date,
        "lessonStartTime": singteach.start_time,
        "lessonEndTime": singteach.end_time,
        "lessonduration": singteach.lesson_duration,
        "class_id": singteach.class_id,
        "checkRuels": ["available", "subject", "cons_class", "same_class", "last_7d_relief"],
        "row_id": singteach.absence_request_table_id,
        "assign_id": singteach.assigned_id,
        "status_id": singteach.status_id
      }
    });

    modal.onDidDismiss().then(data => {
      this.selectedteac = JSON.parse(localStorage.getItem('selectteach'));
      this.assignments = [];
      this.presentLoading();
      this.getassignments(this.user.data.user_id);
    });
    return await modal.present();
  }

  async history() {
    const modal = await this.modalCtrl.create({
      component: HistoryComponent,
      cssClass: 'historyModal'
    });

    return await modal.present();
  }

  async changeTea() {
    const modal = await this.modalCtrl.create({
      component: ChangeTeaComponent,
      cssClass: 'decisionModal'
    });

    return await modal.present();
  }

  logout() {
    this.loginService.logOut();
  }

}
