import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AssignmentPageRoutingModule } from './assignment-routing.module';

import { AssignmentPage } from './assignment.page';
import { HistoryComponent } from './../../components/assignment/history/history.component';
import { ChangeTeaComponent } from './../../components/assignment/change-tea/change-tea.component';
import { AvailableComponent } from './../../components/assignment/available/available.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AssignmentPageRoutingModule
  ],
  declarations: [AssignmentPage, HistoryComponent, ChangeTeaComponent, AvailableComponent],
  entryComponents: [HistoryComponent, ChangeTeaComponent, AvailableComponent]
})
export class AssignmentPageModule { }
