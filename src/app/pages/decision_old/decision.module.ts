import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DecisionPageRoutingModule } from './decision-routing.module';
import { DecisionPage } from './decision.page';
import { FileViewComponent } from './../../components/file-view/file-view.component';
import { RejectComponent } from './../../components/assignment/reject/reject.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DecisionPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [DecisionPage, FileViewComponent, RejectComponent],
  entryComponents: [FileViewComponent, RejectComponent]
})
export class DecisionPageModule { }
