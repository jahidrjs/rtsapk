import { LeaveTypeService } from './../../services/request/leave-type.service';
import { RetriveService } from './../../services/request/retrive.service';
import { LoginService } from './../../services/login.service';
import { TeacherService } from './../../services/request/teacher.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, LoadingController, AlertController } from '@ionic/angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { IonBackButtonDelegate } from '@ionic/angular';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
  providers: [DatePipe]
})
export class RequestPage implements OnInit {
  @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;
  requestForm: FormGroup;
  user_name: string;
  user_phon: string;
  user_id: string;
  item: any = [];
  leaveTypes: any = [];

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private retrieveData: RetriveService,
    private datepipe: DatePipe,
    private getleave: LeaveTypeService,
    private teacherServ: TeacherService,
    private loginService: LoginService,
    public loadingController: LoadingController,
    public alertShow: AlertController
  ) { }

  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    this.setUIBackButtonAction();
  }

  setUIBackButtonAction() {
    this.backButton.onClick = () => {
      this.router.navigate(['/dashboard']);
    };
  }

  ngOnInit() {
    this.menuCtrl.enable(true);
    this.initRequestForm();
    this.basicInfo();
    this.leaveList();
    this.teacher_list();
    this.clearRequest();
  }

  basicInfo() {
    this.item = JSON.parse(localStorage.getItem('currentUser'));
    this.user_name = this.item.data.user_full_name;
    this.user_phon = this.item.data.phone_number;
    this.user_id = this.item.data.user_id;
  }

  logout() {
    this.loginService.logOut();
  }

  initRequestForm() {
    this.requestForm = new FormGroup({
      leaveType: new FormControl(''),
      startDate: new FormControl('', Validators.required),
      endDate: new FormControl('', Validators.required),
    });
  }

  getData() {
    const search: any = {};
    search.userid = this.user_id; // this.requestForm.value.phone;
    search.start_date = this.datepipe.transform(this.requestForm.value.startDate, 'dd/MM/yyyy');
    search.end_date = this.datepipe.transform(this.requestForm.value.endDate, 'dd/MM/yyyy');
    search.leave_type = this.requestForm.value.leaveType;
    return search;
  }

  async presentAlert(msg) {
    const alert = await this.alertShow.create({
      // cssClass: 'my-custom-class',
      header: '',
      // subHeader: 'Subtitle',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  retrive() {
    console.log(this.getData());
    localStorage.removeItem('retrieveData');
    this.presentLoading();
    this.retrieveData.retriveData(this.getData()).subscribe((data: any) => {
      this.getRetrieveData(data);
    });
  }

  getRetrieveData(data) {
    localStorage.setItem('searchDate', JSON.stringify(this.getData()));
    if (data.length > 0) {
      this.router.navigate(['/retrive']);
      this.loadingdismiss();
    } else {
      this.router.navigate(['/request']);
      this.loadingdismiss();
    }
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();
  }

  async loadingdismiss() {
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  getLeaveType() {
    this.leaveTypes = JSON.parse(localStorage.getItem('leaveType'));
  }

  leaveList() {
    this.getleave.retriveData().subscribe((data: any) => {
      console.log(data);
    });
  }

  teacher_list() {
    this.teacherServ.retriveData(this.getData()).subscribe((data: any) => {
      console.log(data);
    });
  }

  clearRequest() {
    localStorage.removeItem('searchDate');
  }
}
