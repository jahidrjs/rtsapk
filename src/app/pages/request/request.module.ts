import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormControl, Validators, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RequestPageRoutingModule } from './request-routing.module';
import { RequestPage } from './request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RequestPageRoutingModule
  ],
  declarations: [RequestPage]
})
export class RequestPageModule implements OnInit {
  requestForm: FormGroup;
  constructor() { }
  // tslint:disable-next-line: contextual-lifecycle
  ngOnInit() {
    this.requestFormInit();
  }
  requestFormInit() {
    /*
    this.requestForm = new FormGroup({
      teacherName: new FormControl(''),
      leaveType: new FormControl(''),
      startDate: new FormControl('', [Validators.required]),
      endDate: new FormControl('', [Validators.required]),
      reasonDesc: new FormControl('', [Validators.required])
    });
    */
  }
}
