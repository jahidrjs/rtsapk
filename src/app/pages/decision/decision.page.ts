import { FilePreviewService } from './../../services/decision/file-preview.service';
import { RejectAssignmentService } from './../../services/decision/reject-assignment.service';
import { AcceptAssignmentService } from './../../services/decision/accept-assignment.service';
import { RejectComponent } from './../../components/assignment/reject/reject.component';
import { LoginService } from './../../services/login.service';
import { FileViewComponent } from './../../components/file-view/file-view.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, ModalController, AlertController } from '@ionic/angular';
import { RedirectUrlService } from 'src/app/services/redirect-url.service';
import { AssignmentListService } from 'src/app/services/decision/assignment-list.service';
import { LoadingController } from '@ionic/angular';
import { SelectiveLoadingStrategy } from 'src/app/guards/selective-loading-strategy';
import { IonBackButtonDelegate } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-decision',
  templateUrl: './decision.page.html',
  styleUrls: ['./decision.page.scss'],
})
export class DecisionPage implements OnInit {
  @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;
  user_name: string;
  user_phon: string;
  user_id: string;
  item: any = [];
  retValues: any = [];
  fileCheck: any = [];

  constructor(
    private router: Router,
    private menu: MenuController,
    public modalCtrl: ModalController,
    private loginService: LoginService,
    private redirectTo: RedirectUrlService,
    private fileView: FilePreviewService,
    private rejectAssign: RejectAssignmentService,
    private accetAssign: AcceptAssignmentService,
    private assignmentList: AssignmentListService,
    public loadingController: LoadingController,
    private alertShow: AlertController,
    private loader: SelectiveLoadingStrategy
  ) { }

  ngOnInit() {
    this.loader.preLoadRoute('decision');
    this.menu.enable(true);
    this.redirectTo.redirectTO();
    this.basicInfo();
    this.presentLoading();
    setTimeout(() => {
      this.assinmentList();
    }, 1000);
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    this.setUIBackButtonAction();
  }
  setUIBackButtonAction() {
    this.backButton.onClick = () => {
      this.router.navigate(['/dashboard']);
    };
  }

  initUser() {
    const userInfo: any = {};
    userInfo.userid = this.user_id;
    return userInfo;
  }

  assinmentList() {
    // console.log(this.initUser());
    // this.presentLoading();
    this.assignmentList.retriveData(this.initUser()).subscribe((data: any) => {
      this.retValues = JSON.parse(localStorage.getItem('assignmentList'));
      this.loadingdismiss();
      console.log(this.retValues);
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      // cssClass: 'my-custom-class',
      message: 'Please wait...',
      // duration: 2000
    });
    await loading.present();
  }

  async loadingdismiss() {
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async dismiss() {
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  logout() {
    this.loginService.logOut();
  }

  basicInfo() {
    this.item = JSON.parse(localStorage.getItem('currentUser'));
    this.user_name = this.item.data.user_full_name;
    this.user_phon = this.item.data.phone_number;
    this.user_id = this.item.data.user_id;
  }

  async openFileModal(val, row_id) {
    if (val === 'document') {
      const modal = await this.modalCtrl.create({
        component: FileViewComponent,
        cssClass: 'decisionModal',
        componentProps: {
          "row_id": row_id
        }
      });
      return await modal.present();
    } else if (val === 'reject') {
      const modal = await this.modalCtrl.create({
        component: RejectComponent,
        cssClass: 'rejectModal',
        componentProps: {
          "row_id": row_id,
          "user_id": this.user_id
        }
      });
      return await modal.present();
    }
  }

  async presentAlert(msg) {
    const alert = await this.alertShow.create({
      message: msg,
      buttons: ['OK']
    });
    await alert.present();
  }

  getUser(val) {
    const userInfo: any = {};
    userInfo.userid = this.user_id;
    userInfo.row_id = val;
    console.log(userInfo);
    return userInfo;
  }

  assignAccept(val) {
    this.presentLoading();
    this.accetAssign.retriveData(this.getUser(val)).subscribe((data: any) => {
      console.log(data);
      this.presentAlert(data);
      this.dismiss();
      this.assinmentList();
      this.router.navigate(['/decision']);
    });
  }
}
