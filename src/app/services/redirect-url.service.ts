import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RedirectUrlService {
  path: any;
  constructor(private router: Router) { }
  redirectTO() {
    if (localStorage.length === 0) {
      this.router.navigate(['/']);
    } else {
      this.path = window.location.pathname;
      this.router.navigate([this.path]);
    }
  }
}
