import { TestBed } from '@angular/core/testing';

import { RetriveService } from './retrive.service';

describe('RetriveService', () => {
  let service: RetriveService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RetriveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
