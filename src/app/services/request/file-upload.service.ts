import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RedirectUrlService } from '../redirect-url.service';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  private baseUrl = environment.apiUrl;
  public localData: any = [];

  constructor(private http: HttpClient, private redirectTo: RedirectUrlService) { }
  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  // tslint:disable-next-line: ban-types
  filesUpload(data: any) {
    // const existing = localStorage.getItem('uploadFileInfo');
    // this.localData = existing ? existing.split(',') : [];
    return this.http.post<any>(`${this.baseUrl}retrieveFileUpload`, data)
      .pipe(map(uploadedFiles => {
        // this.localData.push(JSON.stringify(uploadedFiles.data));
        // localStorage.setItem('uploadFile', JSON.stringify(uploadedFiles.data));
        return uploadedFiles.data;
      }));
  }
}
