import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RedirectUrlService } from '../redirect-url.service';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RetriveService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient, private redirectTo: RedirectUrlService) { }
  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  // tslint:disable-next-line: ban-types
  retriveData(data: any) {
    return this.http.post<any>(`${this.baseUrl}retrieve_lessons`, data)
      .pipe(map(retrieve => {
        if (retrieve && retrieve.status === 1) {
          localStorage.setItem('retrieveData', JSON.stringify(retrieve.data));
        }
        return retrieve.data;
      }));
  }
  submitRequest(data: any) {
    return this.http.post<any>(`${this.baseUrl}save_request`, data)
      .pipe(map(retrieve => {
        if (retrieve && retrieve.status === 1) {
          // localStorage.setItem('retrieveData', JSON.stringify(retrieve.data));
          console.log(retrieve);
        }
        return retrieve;
      }));
  }
}
