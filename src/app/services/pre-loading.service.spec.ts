import { TestBed } from '@angular/core/testing';

import { PreLoadingService } from './pre-loading.service';

describe('PreLoadingService', () => {
  let service: PreLoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreLoadingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
