import { TestBed } from '@angular/core/testing';

import { RejectAssignmentService } from './reject-assignment.service';

describe('RejectAssignmentService', () => {
  let service: RejectAssignmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RejectAssignmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
