import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RedirectUrlService } from '../redirect-url.service';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RejectAssignmentService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient, private redirectTo: RedirectUrlService) { }
  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  // tslint:disable-next-line: ban-types
  retriveRequest(data: any) {
    return this.http.post<any>(`${this.baseUrl}reject_request`, data)
      .pipe(map(rejected => {
        // localStorage.setItem('assignmentList', JSON.stringify(teachers.data));
        return rejected.msg;
      }));
  }
  retriveTypes() {
    return this.http.get<any>(`${this.baseUrl}reject_types`)
      .pipe(map(rejected => {
        localStorage.setItem('rejectTypes', JSON.stringify(rejected.data));
        return rejected.data;
      }));
  }
}
