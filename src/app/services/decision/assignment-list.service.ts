import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RedirectUrlService } from '../redirect-url.service';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AssignmentListService {
  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient, private redirectTo: RedirectUrlService) { }
  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  // tslint:disable-next-line: ban-types
  retriveData(data: any) {
    return this.http.post<any>(`${this.baseUrl}get_relief_data`, data)
      .pipe(map(teachers => {
        localStorage.setItem('assignmentList', JSON.stringify(teachers.data));
        return teachers.data;
      }));
  }

  retriveassign(id) {
    return this.http.post(`${this.baseUrl}retrieve_assignment`, { "userid": id });
  }
  retriveteachers(passdata) {
    return this.http.post(`${this.baseUrl}available_teacher`, passdata);
  }

  selectrecom(recomdata) {
    return this.http.post(`${this.baseUrl}select_recomm`, recomdata);
  }

  sendreq(senddata) {
    return this.http.post(`${this.baseUrl}send_req`, senddata);
  }
}
