import { TestBed } from '@angular/core/testing';

import { AcceptAssignmentService } from './accept-assignment.service';

describe('AcceptAssignmentService', () => {
  let service: AcceptAssignmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcceptAssignmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
