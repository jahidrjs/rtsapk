import { SelectiveLoadingStrategy } from './guards/selective-loading-strategy';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PreLoadingService } from './services/pre-loading.service';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
    data: {
      preload: true
    }
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule),
    data: {
      preload: true, delay: true
    }
  },
  {
    path: 'request',
    loadChildren: () => import('./pages/request/request.module').then(m => m.RequestPageModule),
    data: {
      preload: true, delay: true
    }
  },
  {
    path: 'decision',
    loadChildren: () => import('./pages/decision/decision.module').then(m => m.DecisionPageModule),
    data: {
      name: 'decision', preload: true, delay: true
    }
  },
  {
    path: 'assignment',
    loadChildren: () => import('./pages/assignment/assignment.module').then(m => m.AssignmentPageModule),
    data: {
      preload: true, delay: true
    }
  },
  {
    path: 'retrive',
    loadChildren: () => import('./pages/retrive/retrive.module').then(m => m.RetrivePageModule),
    data: {
      preload: true, delay: true
    }
  }
];

@NgModule({
  providers: [SelectiveLoadingStrategy],
  imports: [
    RouterModule.forRoot(routes, {
    preloadingStrategy: SelectiveLoadingStrategy,
    onSameUrlNavigation: 'reload',
    relativeLinkResolution: 'legacy'
})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
