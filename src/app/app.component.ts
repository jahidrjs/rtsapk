import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, AlertController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public selectedIndex = 0;
  user_name: string;
  user_phon: string;
  hod: boolean = false;
  item: any = [];
  public appPages = [
    {
      title: 'Dashboard',
      url: 'dashboard',
      icon: 'home',
      status: 1,
      id: 1
    },
    {
      title: 'Request for Relief',
      url: 'request',
      icon: 'git-pull-request',
      status: 1,
      id: 2
    },
    {
      title: 'Relief Assignment By HOD / Relief Team',
      url: 'assignment',
      icon: 'list',
      status: 1,
      id: 3
    },
    {
      title: 'Acknowledge / Unable to Relief',
      url: 'decision',
      icon: 'code-working',
      status: 1,
      id: 4
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private alertCtrl: AlertController,
    private menu: MenuController
  ) {
    this.initializeApp();
    setTimeout(() => {
      this.platform.backButton.subscribe(async () => {
        if (this.router.isActive('/login', true) || this.router.isActive('/dashboard', true)) {
          const alert = await this.alertCtrl.create({
            header: 'Exit app ?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              }, {
                text: 'Close',
                handler: () => {
                  navigator["app"].exitApp();
                }
              }
            ]
          });
          if (alert) {
            alert.dismiss();
          }
          await alert.present();
        }
      });
    }, 100);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    setInterval(() => {
      this.basicInfo();
    }, 1 * 1000);
  }
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    const path = window.location.pathname;
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
  updateArray(id, val) {
    var index = this.appPages.findIndex(obj => obj.id === id);
    if (index > -1) {
      this.appPages[index]['status'] = val;
    }
  }
  basicInfo() {
    if (localStorage.length > 0) {
      this.item = JSON.parse(localStorage.getItem('currentUser'));
      this.user_name = this.item.data.user_full_name;
      this.user_phon = this.item.data.phone_number;
      this.hod = (this.item.data.hod === 1) ? true : false;
      if (this.hod == false) {
        this.updateArray(3, 0);
      } else {
        this.updateArray(3, 1);
      }
    } else {
      this.user_name = '';
      this.user_phon = '';
    }
  }
}
